from setuptools import setup, find_packages

setup(
    name='pygments-qsharp',
    version='0.1',

    packages=find_packages(),
    install_requires=['pygments >= 2.3.1'],

    entry_points='''[pygments.lexers]
                    qsharp=pygments_qsharp:QSharpLexer''',
)
