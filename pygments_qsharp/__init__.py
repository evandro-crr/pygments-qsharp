from pygments.lexers.dotnet import FSharpLexer
from pygments.token import Name, Keyword


class QSharpLexer(FSharpLexer):
    name = 'qsharp'
    aliases = ['qsharp']
    filenames = ['*.qs']
    extra_keywords = ['operation', 'Int', 'Unit', 'using',
                      'Qubit', 'repeat', 'until', 'fixup', 'Controlled', 'Zero']

    def get_tokens_unprocessed(self, text):
        for index, token, value in FSharpLexer.get_tokens_unprocessed(self, text):
            if token is Name and value in self.extra_keywords:
                yield index, Keyword, value
            else:
                yield index, token, value
